class LinkFrequencyWeightedIndexedPages extends IndexedPages {

  override def getWeights: Seq[Double] = {
    // base value of 1.0 to avoid zero weights for pages which are relevant to query but not link occurences
    getItems.map(x =>{
      1.0 + getItems.count(y => y.links.contains(x.url)).toDouble
    })
  }

}
