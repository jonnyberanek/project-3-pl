class LengthWeightedIndexedPages extends IndexedPages {

  override def getWeights: Seq[Double] = {
    val maxLength = getItems.maxBy(_.url.length).url.length.toDouble
    getItems.map(x=> maxLength/x.url.length.toDouble)
  }
}
