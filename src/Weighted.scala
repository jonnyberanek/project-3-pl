trait Weighted[A] {

  def getItems: Seq[A]
  def getWeights: Seq[Double]
  
  def sumIf(f: A => Boolean): Double = {
    getItems.zip(getWeights).filter(x=> f(x._1)).map(_._2).sum
  }
}
