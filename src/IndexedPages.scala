class IndexedPages extends Seq[Page] with Weighted[Page] {
  var items:Set[Page]= Set.empty[Page]
  override def getItems: Seq[Page] = items.toSeq

  override def getWeights: Seq[Double] = items.toSeq.map(_ => 1.0)

  override def length: Int = items.size

  override def apply(idx: Int): Page = {
    if(idx<items.size) items.toSeq(idx) else throw new RuntimeException("index out of bounds")
  }

  override def iterator: Iterator[Page] = new MyIter

  def add(item:Page): Unit = {
    // sets cannot contain duplicates and thus 'checks' for them automatically
    items += item
  }

  private class MyIter extends Iterator[Page] {
    var nextIndex = 0

    def next: Page = {
      if(nextIndex< items.size){
        nextIndex += 1
        items.toSeq(nextIndex)
      }else{
        throw new RuntimeException("bad iterator")
      }
    }

    def hasNext: Boolean = { nextIndex < items.size }
  }

  def search(query: Query): SearchResults = {
    //val occurencesMap = getItems.map(x => (x.url,x.tf()))
    val w = getWeights
    val weightedMatchMap = getItems.zipWithIndex.map({case(x,i) => {
      val wordOccurences = x.tf()
      val querySum = query.sumIf(x.tf().keys.toSeq.contains(_))
      (x.url, querySum * w(i))
    }})
    // val matchCount = occurencesMap.map(x => {
    //   (x._1, q.getItems.intersect(x._2.keys.toSeq).length)
    // }).toMap


    // val pagesWithIndex = getItems.zipWithIndex.filter(p => matchCount.contains(p._1.url))
    // val weightedMatchMap = pagesWithIndex.map(x=> (x._1.url,matchCount(x._1.url)*w(x._2)))

    new SearchResults(query,length,weightedMatchMap.map(_._1),weightedMatchMap.map(_._2))
  }
}
