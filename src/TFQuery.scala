class TFQuery(terms:Seq[String]) extends Query(terms) {

  // assigning lesser weight to words with less than 4 letters filters out common articles and shorter prepositions
  //   while not filtering most important query terms (maybe except short acronyms)
  override def getWeights: Seq[Double] = {
    getItems.map(x => if (x.length <= 3) (x, 0.5) else (x, 1.0))
      .groupBy(x => x._1)
      .mapValues(_.map(_._2).sum).values.toSeq
  }

}
