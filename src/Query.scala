class Query(terms:Seq[String]) extends Weighted[String] {
  override def getItems: Seq[String] = terms

  override def getWeights: Seq[Double] = terms.map(_=>1.0)
}
